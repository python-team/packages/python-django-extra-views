python-django-extra-views (0.14.0-4) unstable; urgency=medium

  * Team upload.
  * remove extraneous build-dependencies:
    - python3-django-nose
    - python3-six

 -- Alexandre Detiste <tchet@debian.org>  Fri, 03 May 2024 11:24:01 +0200

python-django-extra-views (0.14.0-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + python-django-extra-views-doc: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 22:01:18 +0100

python-django-extra-views (0.14.0-2) unstable; urgency=medium

  * Use github tags instead of releases for d/watch.
  * Add patch for Django 4 compatibility (Closes: #1013628).
  * Bump Standards-Version to 4.6.1.0.
  * Update year in d/copyright.
  * Depend on python3-all for autopkgtests.

 -- Michael Fladischer <fladi@debian.org>  Thu, 28 Jul 2022 20:34:23 +0000

python-django-extra-views (0.14.0-1) unstable; urgency=low

  [ Diego M. Rodriguez ]
  * d/tests: specify import name

  [ Michael Fladischer ]
  * New upstream release.
  * Update d/watch to work with github again.
  * Bump Standards-Version to 4.6.0.1.
  * Use uscan version 4.
  * Configure tests using pybuild environment variables instead of
    debhelper override.
  * Enable upstream testsuite for autopkgtests.
  * Remove unnecessary autopkgtest-pkg-python testsuite.

 -- Michael Fladischer <fladi@debian.org>  Wed, 20 Oct 2021 12:26:52 +0000

python-django-extra-views (0.13.0-1) unstable; urgency=low

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Michael Fladischer ]
  * New upstream release.
  * Refresh patches.
  * Bump debhelper version to 13.
  * Bump Standards-Version to 4.5.0.
  * Set Rules-Requires-Root: no.
  * Add d/upstream/metadata.
  * Add python3-pytest and python3-pytest-django to Build-Depends,
    required by tests.
  * Use pytest to run tests.
  * Clean up pytest artifacts to allow two builds in a row.

 -- Michael Fladischer <fladi@debian.org>  Fri, 23 Oct 2020 21:18:36 +0200

python-django-extra-views (0.12.0-2) unstable; urgency=medium

  * Team upload.
  * Removed Python 2 support.
  * Switched to using debhelper-compat.

 -- Thomas Goirand <zigo@debian.org>  Fri, 26 Jul 2019 23:05:32 +0200

python-django-extra-views (0.12.0-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 4.2.1.
  * Use 'python3 -m sphinx' instead of 'sphinx-build' when building
    documentation.

 -- Michael Fladischer <fladi@debian.org>  Sat, 27 Oct 2018 13:32:30 +0200

python-django-extra-views (0.11.0-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 4.1.4.
  * Remove X-Python(3)-Version field as it is no longer applicable.

 -- Michael Fladischer <fladi@debian.org>  Sun, 06 May 2018 15:48:43 +0200

python-django-extra-views (0.10.0-1) unstable; urgency=low

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Michael Fladischer ]
  * Refresh patches after git-dpm to gbp pq conversion
  * New upstream release.
  * Bump debhelper compatibility and version to 11.
  * Bump Standards-Version to 4.1.3.
  * Run wrap-and-sort -bast to reduce diff size of future changes.
  * Build documentation in override_dh_sphinxdoc.
  * Enable autopkgtest-pkg-python testsuite.

 -- Michael Fladischer <fladi@debian.org>  Wed, 07 Mar 2018 14:42:53 +0100

python-django-extra-views (0.9.0-1) unstable; urgency=low

  * New upstream release.
  * Drop Django 1.8 compatibility patch, merged upstream.
  * Add patch to prevent network access during build with intersphinx.

 -- Michael Fladischer <fladi@debian.org>  Thu, 16 Mar 2017 11:27:48 +0100

python-django-extra-views (0.8.0-1) unstable; urgency=low

  * Initial release (Closes: #856518).

 -- Michael Fladischer <fladi@debian.org>  Wed, 01 Mar 2017 15:37:25 +0100
